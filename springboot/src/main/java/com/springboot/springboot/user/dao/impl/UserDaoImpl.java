package com.springboot.springboot.user.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.springframework.stereotype.Repository;

import com.bean.UserInformation;
import com.configuration.ConnectionDB;
import com.springboot.springboot.user.dao.UserDao;

/**
 * @author rkritchat
 * @date 12-03-2561
 */
@Repository
public class UserDaoImpl implements UserDao {
	
	@Override
	public String getUserDetail(String id) throws Exception {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = ConnectionDB.getConnection();
			ps = con.prepareStatement("SELECT first_name AS firstName, last_name AS lastName FROM userinfo WHERE id = ?");
			ps.setString(1,id);
			rs = ps.executeQuery();
			if(rs.next()) {
				return "Hi "+rs.getString("firstName") + " " + rs.getString("lastName");
			}else {
				return "Invalid User id";
			}
		}finally {
			ConnectionDB.closeConnection(con, ps, rs);
		}
	}

	@Override
	public String createUser(UserInformation bean) throws Exception {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = ConnectionDB.getConnection();
			ps = con.prepareStatement("INSERT INTO userinfo (id,first_name,last_name,email) VALUES (?,?,?,?)");
			ps.setString(1, bean.getId());
			ps.setString(2, bean.getFirstName());
			ps.setString(3, bean.getLastName());
			ps.setString(4, bean.getEmail());
			ps.executeQuery();
			con.commit();
		}catch(Exception exception) {
			System.out.println(exception);
			con.rollback();
		}finally{
			ConnectionDB.closeConnection(con, ps, null);
		}
		return "User "+ bean.getFirstName() + " was created successfully.";
	}

	@Override
	public String updateUserDetail(UserInformation bean) throws Exception {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = ConnectionDB.getConnection();
			ps = con.prepareStatement("UPDATE userInfo SET first_name = ? WHERE id = ?");
			ps.setString(1, bean.getFirstName());
			ps.setString(2, bean.getId());
			ps.executeQuery();
			con.commit();
		}catch(Exception exception) {
			System.out.println(exception);
			con.rollback();
		}finally {
			ConnectionDB.closeConnection(con, ps, null);
		}
		return "User "+ bean.getFirstName() + " was updated successfully.";
	}

	@Override
	public String deleteUser(UserInformation bean) throws Exception {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = ConnectionDB.getConnection();
			ps = con.prepareStatement("DELETE userinfo WHERE id = ? ");
			ps.setString(1,bean.getId());
			ps.executeQuery();
			con.commit();
		}catch(Exception exception) {
			System.out.println(exception);
			con.rollback();
		}finally {
			ConnectionDB.closeConnection(con, ps, null);
		}
		return "User id :"+ bean.getId() + " was deleted successfully.";
	}

}
