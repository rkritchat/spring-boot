package com.springboot.springboot.user.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.UserInformation;
import com.springboot.springboot.user.dao.UserDao;
import com.springboot.springboot.user.service.UserService;

/**
 * @author rkritchat
 * @date 12-03-2561
 */
@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserDao userDao;
	
	@Override
	public String getUserDetail(String id) {
		try {
			return userDao.getUserDetail(id);
		}catch(Exception exception) {
			System.out.println(exception);
			return "Error while fetch user's detail";
		}
	}

	@Override
	public String createUser(UserInformation bean) {
		try {
			return userDao.createUser(bean);
		}catch(Exception exception) {
			System.out.println(exception);
			return "Error while create user";
		}
	}

	@Override
	public String updateUserDetail(UserInformation bean) {
		try {
			return userDao.updateUserDetail(bean);
		}catch(Exception exception) {
			System.out.println(exception);
			return "Error while update user";
		}
	}

	@Override
	public String deleteUser(UserInformation bean) {
		try {
			return userDao.deleteUser(bean);
		}catch(Exception exception) {
			System.out.println(exception);
			return "Error while delete user";
		}	
	}
}
