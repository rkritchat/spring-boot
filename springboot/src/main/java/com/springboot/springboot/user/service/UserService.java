package com.springboot.springboot.user.service;

import com.bean.UserInformation;

/**
 * @author rkritchat
 * @date 12-03-2561
 */
public interface UserService {
	public String getUserDetail(String id);
	public String createUser(UserInformation bean);
	public String updateUserDetail(UserInformation bean);
	public String deleteUser(UserInformation bean);
}
