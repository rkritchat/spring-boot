package com.springboot.springboot.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bean.UserInformation;
import com.springboot.springboot.user.service.UserService;

/**
 * @author rkritchat
 * @date 12-03-2561
 */
@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private  UserService userService;
	
	@GetMapping
	public String getUerDetail(@RequestParam String id) {
		return userService.getUserDetail(id);
	}
	
	@PutMapping
	public String createUser(@RequestBody UserInformation bean) {
		return userService.createUser(bean);
	}
	
	@PostMapping
	public String updateUserDetail(@RequestBody UserInformation bean) {
		return userService.updateUserDetail(bean);
	}
	
	@DeleteMapping
	public String deleteUser(@RequestBody UserInformation bean) {
		return userService.deleteUser(bean);
	}
}
