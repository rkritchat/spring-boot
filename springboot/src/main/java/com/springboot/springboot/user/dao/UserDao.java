package com.springboot.springboot.user.dao;

import com.bean.UserInformation;

/**
 * @author rkritchat
 * @date 12-03-2561
 */
public interface UserDao {
	public String getUserDetail(String id) throws Exception;
	public String createUser(UserInformation bean) throws Exception;
	public String updateUserDetail(UserInformation bean) throws Exception;
	public String deleteUser(UserInformation bean) throws Exception;
}
