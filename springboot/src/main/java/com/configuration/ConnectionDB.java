package com.configuration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author rkritchat
 * @date 12-03-2561
 */
public class ConnectionDB {
	private static String url = "jdbc:oracle:thin:@localhost:1521:xe";
	private static String user = "system";
	private static String pwd = "oracle";
	
	/**
	 * This method use for get Connection
	 * @return
	 */
	public static Connection getConnection() {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			return DriverManager.getConnection(url,user,pwd);
		} catch (SQLException | ClassNotFoundException exception) {
			System.out.println(exception);
		}
		return null;
	}
		
	/**
	 * This method use for close Connection, PrepareStatement and ResultSet
	 * @param con
	 * @param ps
	 * @param rs
	 * @throws SQLException
	 */
	public static void closeConnection(Connection con, PreparedStatement ps, ResultSet rs) throws SQLException {
		if(con!=null) con.close();
		if(ps!=null) ps.close();
		if(rs!=null) rs.close();
	}

}
