Spring Boot Project
	This project show how to implement Spring Boot for handler the request.
	
	- Method GET
		This method for getUserDetail.
		java method : getUserDetail(String id)
		param : String id
		result : String
		url = localhost:8181/user?id=rkritchat
		
	- Method PUT
		This method for create new user.
		java method : createUser(UserInformation bean)
		param type : JSON
		param : 
			{
				"id":"rkritchat",
				"firstName":"Kritchat",
				"lastName":"Rojanaphruk",
				"email":"rkritchat@gmail.com"
			}
		result : String
		url : localhost:8181/user
	
	- Method POST
		This method for update name by id.
		java method : updateUserDetail(UserInformation bean)
		param type : JSON
		param : 
			{
				"id":"rkritchat",
				"firstName":"Kritchat"
			}
		result : String
		url : localhost:8181/user
		method : POST
		
	- Method DELETE
		This method for delete user from database, delete by id.
		java method : deleteUser(UserInformation bean)
		param type : JSON
		param : 
			{
				"id":"rkritchat"
			}
		result : String
		url : localhost:8181/user
		method : DELETE
		
	 Bean
	 - UserInformation
	     - id
		 - firstName
		 - lastName
		 - eamil
		 - getter/setter/toString
		 
	 Database : Oracle 10g
	 Driver : oracle.jdbc.driver.OracleDriver
	 Database detail
		 - id
		 - first_name
		 - last_name
		 - email
	 Database script
		CREATE TABLE userinfo (id VARCHAR2(50),first_name VARCHAR2(50),last_name VARCHAR2(50),email VARCHAR2(50),primary key(id))
	 